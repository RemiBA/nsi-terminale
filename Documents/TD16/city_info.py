import requests

def get_json(api_key, city):
    """
    Renvoie un dictionnaire contenant les coordonées d'une ville
    ainsi que d'autres informations sur celle-ci.
    
    :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
    :param city: (str) Une ville
    :return: (list) Un dictionnaire
    """
    
    url = f"https://api.openweathermap.org/geo/1.0/direct?q={city}&appid={api_key}"
    r = requests.get(url)
    return r.json()

def get_coordinates(api_key, city):
    """
    Renvoie un tuple contenant les coordonées d'une ville.
    Ces coordonées sont extraites d'un dictionnaire renvoyé par la fonction get_json.
    
    :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
    :param city: (str) Une ville
    :return: (tuple) Un tuple
    """
    
    r = get_json(api_key, city)
    lat = r[0]['lat']
    lon = r[0]['lon']
    return lat, lon