import city_info
import requests


class Weather():
    """
    Classe contenant des fonctions en rapport avec la meteo d'une ville.
    """
    
    def get_city_weather(self, api_key, city):
        """
        Renvoie un dictionnaire contenant des informations sur la météo d'une ville.
        
        :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
        :param city: (str) Une ville
        :return: (list) Un dictionnaire contenant des informations sur la météo d'une ville
        """
        
        coordinates = city_info.get_coordinates(api_key, city)
        url = f"https://api.openweathermap.org/data/2.5/weather?lat={coordinates[0]}&lon={coordinates[1]}&appid={api_key}"
        r = requests.get(url)
        return r.json()

    
    def get_city_temperature(self, api_key, city):
        """
        Renvoie la température d'une ville.
        
        :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
        :param city: (str) Une ville
        :return: (str) La température d'une ville
        """
        
        weather = self.get_city_weather(api_key, city)
        temperature = weather['main']['temp']-273.15
        return "La temperature à " + city + " est de " + str(int(temperature)) + "°C."

    
    def get_city_pressure(self, api_key, city):
        """
        Renvoie la pression atmosphérique d'une ville.
        
        :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
        :param city: (str) Une ville
        :return: (str) La pression atmosphérique d'une ville
        """
        
        weather = self.get_city_weather(api_key, city)
        pressure = weather['main']['pressure']
        return "La pression à " + city + " est de " + str(int(pressure)) + "hPa."

    
    def get_city_humidity(self, api_key, city):
        """
        Renvoie l'humidité atmosphérique d'une ville.
        
        :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
        :param city: (str) Une ville
        :return: (str) L'humidité atmosphérique d'une ville
        """
        
        weather = self.get_city_weather(api_key, city)
        humidity = weather['main']['humidity']
        return "L'humidité à " + city + " est de " + str(int(humidity)) + "%."




class AirPollution():
    """
    Classe contenant des fonctions en rapport avec la pollution atmosphérique d'une ville.
    """
    
    def get_city_air_pollution(self, api_key, city):
        """
        Renvoie un dictionnaire contenant des informations sur la pollution atmosphérique d'une ville.
        
        :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
        :param city: (str) Une ville
        :return: (list) Un dictionnaire contenant des informations sur la pollution atmosphérique d'une ville.
        """
        
        coordinates = city_info.get_coordinates(api_key, city)
        url = f"https://api.openweathermap.org/data/2.5/air_pollution?lat={coordinates[0]}&lon={coordinates[1]}&appid={api_key}"
        r = requests.get(url)
        return r.json()
    
    
    def get_city_air_quality(self, api_key, city):
        """
        Renvoie la qualité de l'air d'une ville.
        
        :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
        :param city: (str) Une ville
        :return: (str) La qualité de l'air d'une ville
        """
        
        air_pollution = self.get_city_air_pollution(api_key, city)
        air_quality = air_pollution['list'][0]['main']['aqi']
        return "La qualité de l'air à " + city + " est de " + str(air_quality) + "."
    
    
    def get_city_CO_concentration(self, api_key, city):
        """
        Renvoie la concentration en CO de l'air d'une ville.
        
        :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
        :param city: (str) Une ville
        :return: (str) La concentration en CO de l'air d'une ville
        """
        
        air_pollution = self.get_city_air_pollution(api_key, city)
        CO_concentration = air_pollution['list'][0]['components']['co']
        return "La concentration en CO à " + city + " est de " + str(CO_concentration) + "μg/m3."
    
    
    def get_city_NO_concentration(self, api_key, city):
        """
        Renvoie la concentration en NO de l'air d'une ville.
        
        :param api_key: (str) Clé permettant d'utiliser les services d'openweathermap
        :param city: (str) Une ville
        :return: (str) La concentration en NO de l'air d'une ville
        """
        
        air_pollution = self.get_city_air_pollution(api_key, city)
        NO_concentration = air_pollution['list'][0]['components']['no']
        return "La concentration en NO à " + city + " est de " + str(NO_concentration) + "μg/m3."



# Permet d'utiliser les fonctions hors de leurs classes.

instWeather = Weather()
get_city_weather = instWeather.get_city_weather
get_city_temperature = instWeather.get_city_temperature
get_city_pressure = instWeather.get_city_pressure
get_city_humidity = instWeather.get_city_humidity

instAirPollution = AirPollution()
get_city_air_pollution = instAirPollution.get_city_air_pollution
get_city_air_quality = instAirPollution.get_city_air_quality
get_city_CO_concentration = instAirPollution.get_city_CO_concentration
get_city_NO_concentration = instAirPollution.get_city_NO_concentration